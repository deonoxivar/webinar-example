const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const uglify = require("gulp-uglify");
const injectPartials = require('gulp-file-include');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');

/* Target dest for compiled task */
const targetPath = "../public";

/* Path URL list */
const paths = {
  sass:
  {
    main:
      [
        'sass/*.scss'
      ],
    vendor:
      [
        "bootstrap/*.scss",
      ]
  },  
};

/* === Task Cleaner Start === */

/* Clean CSS main */
function clean_sass_main() {
  return gulp.src([
    (targetPath + '/assets/css/main.css'), 
    (targetPath + '/assets/css/maps/main.css.map')
    ], {read: false, allowEmpty: true})
  .pipe(clean({force: true}));
}

/* Clean CSS Vendor */
function clean_sass_vendor() {
  return gulp.src([
    (targetPath + '/assets/css/vendor/*.css'), 
    (targetPath + '/assets/css/vendor/maps/*.css.map')
    ], {read: false, allowEmpty: true})
  .pipe(clean({force: true}));
}

/* === Task Cleaner End === */

/* Task for CSS main.css */
function sass_main() {
  return gulp.src(paths.sass.main)
    .pipe(concat('main.css'))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error scss', sass.logError))
    .pipe(postcss([cssnano()]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(targetPath + '/assets/css'))
    .pipe(browserSync.stream())
}
/* Task for CSS vendor */
function sass_vendor() {
  return gulp.src(paths.sass.vendor)
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error scss', sass.logError))
    .pipe(postcss([cssnano()]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(targetPath + '/assets/css/vendor'))
    .pipe(browserSync.stream())
}




/* Task HTML Build */
function html() {
  return gulp.src(("views/*.html"))
    .pipe(injectPartials({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest((targetPath)))
    .pipe(browserSync.stream())
}

/* Task Watch */
function watcher() {
  gulp.watch('sass/**/*', gulp.series('sass_main'));
  gulp.watch(paths.sass.vendor, gulp.series('sass_vendor'));
}

/* Task BrowserSync */
function initBrowserSync() {
  browserSync.init({
    server: {
      baseDir: [(targetPath)]
    }
  });

  gulp.watch('views/**/*.html', gulp.series(html));
}

/* Task List */
gulp.task('sass_main', gulp.series(clean_sass_main, sass_main));
gulp.task('sass_vendor', gulp.series(clean_sass_vendor, sass_vendor));

/* === Init Task === */
gulp.task('init', gulp.parallel('sass_main', 'sass_vendor'));



/* FE Slicing Commmand type "gulp" */
exports.default = gulp.parallel('init', html, watcher, initBrowserSync);
